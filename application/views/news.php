<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="jumbotron">

	<div class="container">

		<h2>CodeIgniter 3 + Bootstrap</h2>

		<p>This example is a quick exercise to illustrate how Bootstrap can be integrated with CodeIgniter.
			Includes a simple login form which autheticates users against a mysql database. Also contains a basic
			news section which pulls items from a database.
		</p>

		<p>
				<a href="https://codeigniter.com/user_guide/general/welcome.html" target="_blank" class="btn btn-lg btn-default">CodeIgniter Documentation</a>
				<a href="http://getbootstrap.com/css/" target="_blank" class="btn btn btn-lg btn-default">Bootstrap Documentation</a>
		</p>


	</div> <!-- /container -->

</div> <!-- /jumbotron -->

<div class="container">

	<div class="col-sm-8 news-content">

		<?php foreach ($news as $news_item): ?>

		        <h3><?php echo $news_item['title']; ?></h3>
						<p class="news-meta"><?php echo date("F d, Y", strtotime($news_item['date']));?> by <a href="#"><?php echo $news_item['author']; ?></a></p>
		        <div class="main">
		                <?php echo $news_item['text']; ?>
		        </div>
		        <p><a href="<?php echo SITEURL.'news/'.$news_item['id']; ?>">View article</a></p>

		<?php endforeach; ?>

	</div>

	<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <h4>About</h4>
            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
          </div>
          <div class="sidebar-module">
            <h4>Archives</h4>
            <ol class="list-unstyled">
              <li><a href="#">March 2014</a></li>
              <li><a href="#">February 2014</a></li>
              <li><a href="#">January 2014</a></li>
              <li><a href="#">December 2013</a></li>
              <li><a href="#">November 2013</a></li>
              <li><a href="#">October 2013</a></li>
              <li><a href="#">September 2013</a></li>
              <li><a href="#">August 2013</a></li>
              <li><a href="#">July 2013</a></li>
              <li><a href="#">June 2013</a></li>
              <li><a href="#">May 2013</a></li>
              <li><a href="#">April 2013</a></li>
            </ol>
          </div>
          <div class="sidebar-module">
            <h4>Elsewhere</h4>
            <ol class="list-unstyled">
              <li><a href="#">GitHub</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Facebook</a></li>
            </ol>
          </div>
        </div>

</div>
