<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

	<div class="container">

		<!--<form class="form-signin">-->
		<?php echo form_open('auth/validate_credentials', 'class="form-signin"'); ?>
		<?php
			if (isset($error_message)) {
			echo '<p class="text-primary">'.$error_message.'</p>';
			}
			echo '<p class="text-primary">'.validation_errors().'</p>';

			?>

			<h3 class="form-signin-heading text-muted">Sign In</h3>
			<label for="username" class="sr-only">Username</label>
			<input type="text" name="username" id="username" class="form-control" placeholder="Email" required autofocus>
			<label for="password" class="sr-only">Password</label>
			<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
			<div class="checkbox">
				<label>
            <input type="checkbox" value="remember-me"> Remember me
        </label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		<!--</form>-->
		<?php echo form_close(); ?>


	</div>
