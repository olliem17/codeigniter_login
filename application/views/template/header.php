<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo SITEURL.'/assets/images/favicon.ico';?>">

    <title><?php echo $title;?></title>

    <!-- Bootstrap -->
    <link href="<?php echo SITEURL.'/assets/css/bootstrap.min.css';?>" rel="stylesheet">

    <!-- Custom styles -->
    <link href="<?php echo SITEURL.'/assets/css/style.css';?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">CodeIgniter 3</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="<?php if($active == 'home') {echo 'active';}?>"><a href="<?php echo SITEURL;?>">Home <span class="sr-only">(current)</span></a></li>
        <li ><a href="#">Features</a></li>
        <li class="<?php if($active == 'news') {echo 'active';}?>"><a href="<?php echo SITEURL.'news';?>">News</a></li>
      </ul>
      <!--<form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>-->
      <ul class="nav navbar-nav navbar-right">



        <?php if(isset($this->session->userdata['logged_in'])) : ?>
        <p class="navbar-text">Signed in as <a href="#" class="navbar-link blue"><?php echo $this->session->userdata['logged_in']['name']; ?></a></p>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Account <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo SITEURL.'admin';?>">Dashboard</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Settings</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo SITEURL.'admin/addnews';?>">Add News</a></li>
            <li><a href="<?php echo SITEURL.'admin/users';?>">Add Account</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo SITEURL.'auth/signout';?>">Sign Out</a></li> 
          </ul>
        </li>

        <?php else : ?>

        <li class="<?php if($active == 'login') {echo 'active';}?>"><a href="<?php echo SITEURL.'auth/signin';?>">Sign In</a></li>

        <?php endif; ?>


      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
