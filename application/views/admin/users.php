<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container">

	<div class="col-sm-8 news-content">

		        <h3>Users</h3>

						<table class="table table-striped">
							<thead>
					      <tr>
									<th>#</th>
					        <th>Name</th>
					        <th>Username</th>
					        <th>Email</th>
									<th>Password</th>
									<th></th>
					      </tr>
					    </thead>

							<tbody>

								<?php foreach ($users as $user): ?>

									<tr>
									 <td><?php echo $user['id']; ?></td>
									 <td><?php echo $user['name']; ?></td>
									 <td><?php echo $user['username']; ?></td>
									 <td><?php echo $user['email']; ?></td>
									 <td><?php echo $user['password']; ?></td>
									 <td><input type="radio" id="radio-1" name="radio" value="1"></td>
								 </tr>

								<?php endforeach; ?>

					    </tbody>
						</table>

						<a href="#" class="btn btn-sm btn-default">Edit</a>
						<a href="#" class="btn btn-sm btn-default">Delete</a>
           <hr/>

					 <?php
						 if (isset($message)) {
							 if ($success) {
								 echo '<div class="alert alert-success" role="alert">'.$message.'</div>';
							 } else {
								 echo '<div class="alert alert-danger" role="alert">'.$message.'</div>';}
						 }
						 if (validation_errors()) {
							 echo '<div class="alert alert-danger" role="alert">'.validation_errors().'</div>';
						 }
						 ?>

					 <div class="panel panel-default">
				 			<div class="panel-heading">
					 				 <h3 class="panel-title">Add User</h3>
				 			 </div>

							<div class="panel-body">

						<?php echo form_open('admin/add_users', 'class="form-horizontal"'); ?>

							<div class="form-group">
								<label for="username" class="control-label col-xs-2">Username</label>
								<div class="col-xs-10">
									<input type="text" name="username" id="username" class="form-control" placeholder="Username" required autofocus>
								</div>
							</div>
							<div class="form-group">
								<label for="password" class="control-label col-xs-2">Password</label>
								<div class="col-xs-10">
									<input type="text" name="password" id="password" class="form-control" placeholder="Password" required>
								</div>
							</div>
							<div class="form-group">
								<label for="name" class="control-label col-xs-2">Name</label>
								<div class="col-xs-10">
									<input type="text" name="name" id="name" class="form-control" placeholder="Name" required>
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="control-label col-xs-2">Email</label>
								<div class="col-xs-10">
									<input type="email" name="email" id="email" class="form-control" placeholder="email" required>
								</div>
							</div>

							<button class="btn btn-sm btn-default" type="submit">Add User</button>

						<!--</form>-->
						<?php echo form_close(); ?>

					</div>
				 </div>

	</div>

	<div class="col-sm-3 col-sm-offset-1 blog-sidebar">

        <div class="sidebar-module">
            <h4>Extra Actions</h4>
            <ol class="list-unstyled">
              <li><a href="#">Add</a></li>
              <li><a href="#">View</a></li>
              <li><a href="#">Edit</a></li>
            </ol>
      </div>
  </div>

</div>
