<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container">

	<div class="col-sm-8 news-content">
		<h3>Admin</h3>
		<?php
		 if (isset($message)) {
			 if ($success) {
				 echo '<div class="alert alert-success" role="alert">'.$message.'</div>';
			 } else {
				 echo '<div class="alert alert-danger" role="alert">'.$message.'</div>';}
		 }
		 if (validation_errors()) {
			 echo '<div class="alert alert-danger" role="alert">'.validation_errors().'</div>';
		 }
		 ?>

	 <div class="panel panel-default">
			<div class="panel-heading">
					 <h3 class="panel-title">Add News</h3>
			 </div>

			<div class="panel-body">

		<?php echo form_open('admin/add_news', 'class="form-horizontal"'); ?>

			<div class="form-group">
				<label for="username" class="control-label col-xs-2">Title</label>
				<div class="col-xs-10">
					<input type="text" name="title" id="title" class="form-control" placeholder="Title" required autofocus>
				</div>
			</div>
			<div class="form-group">
				<label for="name" class="control-label col-xs-2">Author</label>
				<div class="col-xs-10">
					<input type="text" name="author" id="author" class="form-control" placeholder="Author" required>
				</div>
			</div>
			<div class="form-group">
				<label for="text" class="control-label col-xs-2">Article</label>
				<div class="col-xs-10">
					<textarea rows="10" name="text" id="text" class="form-control" placeholder="Article text" required></textarea>
				</div>
			</div>



			<button class="btn btn-sm btn-default" type="submit">Add Article</button>

		<!--</form>-->
		<?php echo form_close(); ?>

	</div>
 </div>

	</div>

	<div class="col-sm-3 col-sm-offset-1 blog-sidebar">

        <div class="sidebar-module">
            <h4>Extra Actions</h4>
            <ol class="list-unstyled">
              <li><a href="#">Add</a></li>
              <li><a href="#">View</a></li>
              <li><a href="#">Edit</a></li>
            </ol>
      </div>
  </div>

</div>
