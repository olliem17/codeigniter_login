<?php

/**
 *
 */

class News_Model extends CI_Model
{

  function __construct()
  {
    $this->load->database();
  }

  public function get_news($id = FALSE)
  {
        if ($id === FALSE)
        {
                $query = $this->db->get('news');
                return $query->result_array();
        }

        $query = $this->db->get_where('news', array('id' => $id));
        return $query->row_array();
  }

  public function add_news ($data)
  {
        $data['date'] = date("Y-m-d");
        
        $this->db->insert('news', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }


  }



}









 ?>
