<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                // Load url helper
                $this->load->helper('url');

                // Load form helper library
                $this->load->helper('form');

                // Load form validation library
                $this->load->library('form_validation');

                // Load session library
                $this->load->library('session');

                // Load login database model
                $this->load->model('Auth_Model');

                // Load News model
                $this->load->model('News_Model');
        }



        public function index()
        {
                if (isset($this->session->userdata['logged_in'])) {
                  $data['title'] = 'CodeIgniter 3 + Bootstrap 3';
                  $data['active'] = '';
  								$this->load->view('template/header', $data);
  			 					$this->load->view('admin/admin');
  			 					$this->load->view('template/footer');
                } else {
                  redirect(SITEURL.'auth/signin');
              }
        }

        public function addnews($message = NULL, $success = NULL)
        {
                if (isset($this->session->userdata['logged_in'])) {
                  $data['title'] = 'CodeIgniter 3 + Bootstrap 3';
                  $data['active'] = '';
                  $data['message'] = $message;
                  $data['success'] = $success;
  								$this->load->view('template/header', $data);
  			 					$this->load->view('admin/addnews');
  			 					$this->load->view('template/footer');
                } else {
                  redirect(SITEURL.'auth/signin');
              }
        }

        public function users($message = NULL, $success = NULL)
        {
                if (isset($this->session->userdata['logged_in'])) {

                  $data['users'] = $this->Auth_Model->get_users();

                  $data['title'] = 'CodeIgniter 3 + Bootstrap 3';
                  $data['active'] = '';
                  $data['message'] = $message;
                  $data['success'] = $success;
  								$this->load->view('template/header', $data);
  			 					$this->load->view('admin/users');
  			 					$this->load->view('template/footer');
                } else {
                  redirect(SITEURL.'auth/signin');
              }
        }


        // Add User Controller
        public function add_users()
        {

          $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[50]');
          $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[50]');

          if ($this->form_validation->run() == FALSE)
          {
              $this->users();
          }
          else
          {

              $data = array(

              'username' => $this->input->post('username'),
              'password' => $this->input->post('password'),
              'name'  => $this->input->post('name'),
              'email'  => $this->input->post('email'));

              $result = $this->Auth_Model->add_user($data);

              if ($result == TRUE) {

                  $this ->users('User added successfully', true);

              } else {

                  $this->users('Error adding user', false);
              }
           }
        }



        // Add User Controller
        public function add_news()
        {

          $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[100]');
          $this->form_validation->set_rules('author', 'Author', 'trim|required|min_length[5]|max_length[50]');
          $this->form_validation->set_rules('text', 'Article', 'trim|required|min_length[5]');

          if ($this->form_validation->run() == FALSE)
          {
              $this->addnews();
          }
          else
          {

              $data = array(

              'title' => $this->input->post('title'),
              'author' => $this->input->post('author'),
              'text'  => $this->input->post('text'));

              $result = $this->News_Model->add_news($data);

              if ($result == TRUE) {

                  $this ->addnews('Article added successfully', true);

              } else {

                  $this->addnews('Error adding article', false);
              }
           }
        }

}

?>
