<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//echo "<script>console.log( 'Debug Objects: " . session_id() . "' );</script>";

class Auth extends CI_Controller {

        public function __construct()
        {
          parent::__construct();

          // Load url helper
          $this->load->helper('url');

          // Load form helper library
          $this->load->helper('form');

          // Load form validation library
          $this->load->library('form_validation');

          // Load session library
          $this->load->library('session');

          // Load login database model
          $this->load->model('Auth_Model');
        }

        // Show login view
        public function signin()
        {
                $data['title'] = 'CodeIgniter 3 + Bootstrap 3';
                $data['active'] = 'login';

                $this->load->view('template/header', $data);
                $this->load->view('login');
                $this->load->view('template/footer');
        }

        // Show login view with message
        public function signin_error($message)
        {
                $data['title'] = 'CodeIgniter 3 + Bootstrap 3';
                $data['active'] = 'login';
                $data['error_message'] = $message;

                $this->load->view('template/header', $data);
                $this->load->view('login');
                $this->load->view('template/footer');
        }

        // Show registration page
        public function register()
        {
                $data['title'] = 'CodeIgniter 3 + Bootstrap 3';
                $data['active'] = 'login';

                $this->load->view('template/header', $data);
                $this->load->view('register');
                $this->load->view('template/footer');
        }

        // Login user
        public function validate_credentials()
        {

          $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[50]');
          $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[50]');

          if ($this->form_validation->run() == FALSE)
          {
              //$this->load->view('myform');
              $this->signin();
          }
          else
          {

              $data = array(
              'username' => $this->input->post('username'),
              'password' => $this->input->post('password'));

              $result = $this->Auth_Model->login($data);

              if ($result == TRUE) {
                  // Login Success
                  $username = $this->input->post('username');

                  $result = $this->Auth_Model->get_details($username);

                  if ($result != false) {

                    $session_data = array(
                    'username' => $result[0]->username,
                    'email' => $result[0]->email,
                    'name' => $result[0]->name,
                    );
                    // Add user data in session
                    $this->session->set_userdata('logged_in', $session_data);
                    // redirect to admin
                    redirect(SITEURL.'admin');

                }

              } else {

                  $this->signin_error('Invalid Username or Password');
              }
           }
        }

        public function signout()
        {
            // Remove session data
            $sess_array = array('username' => '');
            $this->session->unset_userdata('logged_in', $sess_array);

            $this->signin_error('You have signed out.');
        }


}

?>
