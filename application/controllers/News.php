<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('News_Model');
                $this->load->helper('url_helper');
        }

        public function index()
        {
                $data['news'] = $this->News_Model->get_news();
                $data['title'] = 'CodeIgniter 3 + Bootstrap 3';
                $data['active'] = 'news';
								$this->load->view('template/header', $data);
			 					$this->load->view('news', $data);
			 					$this->load->view('template/footer');
        }

        public function view($id = NULL)
        {
                $data['news_item'] = $this->news_model->get_news($id);
        }
}

?>
